from django.shortcuts import render

# Create your views here.
#Command_1
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import QueryDict
import json


@csrf_exempt
def Command_1(request):
    if (request.method == "POST"):
        res = QueryDict(request.body)#.get('asdf')
        print "All ===", res
        print res.get("abc")
        print "Here is POST method..123."
        return JsonResponse({"Result":"POST"})  
    elif (request.method == "GET"):
        res = QueryDict(request.body)
        #......
        print "Here is GET method..."
        return JsonResponse({"Result":"Done"})  
    
    
@csrf_exempt
def Receive_POST_Json(request):
    if (request.method == "POST"):
        print("in Receive_POST_Json")
        print request.body
        res = QueryDict(request.body)
        print "res == ", res
        res = res.get("abc")
        print "res == ", res
        print "Here is Receive_POST_Json method..."
        return JsonResponse({"Result" : res})  
    
@csrf_exempt
def Receive_POST_Rail_Json(request):
    if (request.method == "POST"):
        res = json.loads(request.body)
        res = res["abc"]
        print "Receive_POST_Rail_Json => res", res
        return JsonResponse({"Result" : res})  