import pytest
from django.core.urlresolvers import reverse
from GetInfo import views
import simplejson
from django.test.client import encode_multipart, RequestFactory
from django.http import JsonResponse

def test_Command_1(rf):
    print "test"
    url = "/CMD_1/"
    print rf
    request = rf.get(url)
    response = views.Command_1(request)
    dicRes = simplejson.loads(response.content)
    Result = {"Result" : "Done"}
    print response
    assert response.status_code == 200
    assert response.content == "{\"Result\": \"Done\"}"
    assert dicRes == Result
    
def test_Cmd1_in_Post(rf):
    url = "/CMD_1/"
    print type(rf)
    request = rf.post(url)
    response = views.Command_1(request)
    dicRes = simplejson.loads(response.content)
    Result = {"Result" : "POST"}
    print response
    assert response.status_code == 200
    assert response.content == "{\"Result\": \"POST\"}"
    assert dicRes == Result    

    
def test_Receive_POST_Json(rf):
    url = "/Receive_POST_Json/"
    #data = {"abc" : "def"}
    data = "abc=def;aaa=bbb"
    #data = simplejson.dumps(data)
    #factory = RequestFactory()
    #content = encode_multipart('BoUnDaRyStRiNg', data)
    content_type = 'multipart/form-data; boundary=BoUnDaRyStRiNg'
    request = rf.post(url, data, content_type=content_type)
    
    #request = rf.post(url, data)
    response = views.Receive_POST_Json(request)
    dicRes = simplejson.loads(response.content)
    assert dicRes == {"Result" : "def"}
    

def test_Send_POST_Json(rf):
    url = "/Receive_POST_Rail_Json/"
    data = {"abc" : "def"}
    data = simplejson.dumps(data)
    content_type = 'application/json'
    request = rf.post(url, data, content_type=content_type)
    response = views.Receive_POST_Rail_Json(request)
    dicRes = simplejson.loads(response.content)
    assert dicRes == {"Result" : "def"}    
    



    
if __name__ == "__main__":
    pytest.main()