from django.conf.urls import patterns, include, url
from django.contrib import admin

from GetInfo import views as GetInfo_Views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rest_exam01.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^CMD_1/', GetInfo_Views.Command_1),
    url(r'^Receive_POST_Json/', GetInfo_Views.Receive_POST_Json),
    url(r'^Receive_POST_Rail_Json/', GetInfo_Views.Receive_POST_Rail_Json),
    
    
)
