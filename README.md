##這個專案功能如下

## 使用 Django 當Server 收 REST API
## 使用 py.test framework 來測試 Django 寫出來的 Function



範例下載路徑：
```
$ git clone https://alarm@bitbucket.org/alarm/django_rest_example.git/wiki
```

py.test 範例檔案：
django_rest_example / rest_exam01 / GetInfo / test_views.py

注意事項：
使用 py.test 必須
```
#要先安裝 pytest
pip install pytest

#需要安裝 simplejson
pip install simplejson
```

然後必須要在專案的 root 路徑中建立

```
pytest.ini #檔案
```
注意事項：

* pytest上層路徑被移動時須開啟__pycache__裡的一個compiled python file, 檔案名稱如(test_views.cpython-27-PYTEST)，檔案內為亂碼，有少數幾個明碼，找到原路徑更新之後才可重跑py.test

* plugin django 之後要跑Pytest須先安裝pytest, pytest-django         (pip install pytest 以及 pip install pytest-django)

* check_password(password, encoded, setter=None, preferred='default')                   password裡面放入request得到的明碼， encoded裡放入資料庫抓到的編碼，若對應成功回傳true 否則false   須先宣告from django.contrib.auth.hashers import check_password